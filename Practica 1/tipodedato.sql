-- Tabla de Clientes
CREATE TABLE cliente (
    id_cliente SERIAL PRIMARY KEY,
    id_tienda INTEGER,
    nombre VARCHAR(50),
    apellido VARCHAR(50),
    correo_electronico VARCHAR(100),
    id_direccion INTEGER,
    activo BOOLEAN
);

-- Tabla de Actores
CREATE TABLE actor (
    id_actor SERIAL PRIMARY KEY,
    nombre VARCHAR(50),
    apellido VARCHAR(50),
    ultima_actualizacion TIMESTAMP
);

-- Tabla de Categorías
CREATE TABLE categoria (
    id_categoria SERIAL PRIMARY KEY,
    nombre VARCHAR(50),
    ultima_actualizacion TIMESTAMP
);

-- Tabla de Películas
CREATE TABLE pelicula (
    id_pelicula SERIAL PRIMARY KEY,
    titulo VARCHAR(100),
    descripcion TEXT,
    ano_estreno INTEGER,
    id_idioma INTEGER,
    id_idioma_original INTEGER,
    duracion_alquiler INTEGER,
    tarifa_alquiler DECIMAL(5,2),
    duracion INTEGER,
    costo_reemplazo DECIMAL(5,2),
    clasificacion VARCHAR(5),
    ultima_actualizacion TIMESTAMP,
    caracteristicas_especiales TEXT,
    texto_completo TEXT
);

-- Tabla de Actores en Películas
CREATE TABLE actor_pelicula (
    id_actor INTEGER,
    id_pelicula INTEGER,
    ultima_actualizacion TIMESTAMP,
    PRIMARY KEY (id_actor, id_pelicula)
);

-- Tabla de Categorías de Películas
CREATE TABLE categoria_pelicula (
    id_pelicula INTEGER,
    id_categoria INTEGER,
    ultima_actualizacion TIMESTAMP,
    PRIMARY KEY (id_pelicula, id_categoria)
);

-- Tabla de Direcciones
CREATE TABLE direccion (
    id_direccion SERIAL PRIMARY KEY,
    direccion VARCHAR(100),
    direccion2 VARCHAR(100),
    distrito VARCHAR(50),
    id_ciudad INTEGER,
    codigo_postal VARCHAR(10),
    telefono VARCHAR(20),
    ultima_actualizacion TIMESTAMP
);

-- Tabla de Ciudades
CREATE TABLE ciudad (
    id_ciudad SERIAL PRIMARY KEY,
    ciudad VARCHAR(50),
    id_pais INTEGER,
    ultima_actualizacion TIMESTAMP
);

-- Tabla de Países
CREATE TABLE pais (
    id_pais SERIAL PRIMARY KEY,
    pais VARCHAR(50),
    ultima_actualizacion TIMESTAMP
);

-- Tabla de Inventario
CREATE TABLE inventario (
    id_inventario SERIAL PRIMARY KEY,
    id_pelicula INTEGER,
    id_tienda INTEGER,
    ultima_actualizacion TIMESTAMP
);

-- Tabla de Idiomas
CREATE TABLE idioma (
    id_idioma SERIAL PRIMARY KEY,
    nombre VARCHAR(50),
    ultima_actualizacion TIMESTAMP
);

-- Tabla de Pagos
CREATE TABLE pago (
    id_pago SERIAL PRIMARY KEY,
    id_cliente INTEGER,
    id_personal INTEGER,
    id_alquiler INTEGER,
    monto DECIMAL(5,2),
    fecha_pago TIMESTAMP
);

-- Tabla de Rentas
CREATE TABLE renta (
    id_renta SERIAL PRIMARY KEY,
    fecha_renta TIMESTAMP,
    id_inventario INTEGER,
    id_cliente INTEGER,
    fecha_devolucion TIMESTAMP,
    id_personal INTEGER,
    ultima_actualizacion TIMESTAMP
);

-- Tabla de Personal
CREATE TABLE personal (
    id_personal SERIAL PRIMARY KEY,
    nombre VARCHAR(50),
    apellido VARCHAR(50),
    id_direccion INTEGER,
    correo_electronico VARCHAR(100),
    id_tienda SMALLINT,
    activo BOOLEAN,
    nombre_usuario VARCHAR(50),
    contrasena VARCHAR(50),
    ultima_actualizacion TIMESTAMP
);

-- Tabla de Tiendas
CREATE TABLE tienda (
    id_tienda SERIAL PRIMARY KEY,
    id_personal INTEGER,
    id_direccion INTEGER,
    ultima_actualizacion TIMESTAMP
);
